<?php

namespace Drupal\pants\Tests;
use Drupal\simpletest\WebTestBase;

/**
 * Ensures that an administrator can configure the type of pants used on the site.
 * 
 * @group pants
 */
class PantsConfigurationTestCase extends WebTestBase {

  protected $profile = 'testing';

  /**
   * Admin user.
   */
  protected $admin_user;

  public static $modules = ["pants"];

  public static function getInfo() {
    return array(
      'name' => 'Configuration of pants',
      'description' => "Ensures that an administrator can configure the type of pants used on the site.",
      'group' => 'Pants',
    );
  }

  function setUp() {
    parent::setUp();

    $this->admin_user   = $this->drupalCreateUser(array('administer pants'));
  }

  /**
   * Ensures pants_type setting can be changed.
   */
  function testPantsConfiguration() {
    $this->drupalLogin($this->admin_user);

    $this->drupalGet('admin/config/people/pants');
    $this->assertFieldByName('pants_type', '');

    $this->drupalPostForm(NULL, array('pants_type' => 'bellbottoms'), t('Save configuration'));
    $this->assertFieldByName('pants_type', 'bellbottoms');
  }
}
